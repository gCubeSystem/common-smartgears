# Common Smartgears

A core gCube library which empower a servlet container (e.g. tomcat) with a set of functionality such as:

- node and application infrastructure registration
- authorization
- accounting


## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

[SmartGears](https://wiki.gcube-system.org/gcube/SmartGears)

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/common-smartgears/releases).

## Authors

* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)
* **Lucio Lelii** - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)
* **Fabio Simeoni** - FAO of the UN, Italy


## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?


    @Manual{,
        title = {Common Smartgears},
        author = {{Frosini, Luca}, {Lelii, Lucio}, {Simeoni, Fabio}},
        organization = {{ISTI - CNR}, {FAO}},
        address = {{Pisa, Italy}, {Roma, Italy}},
        year = 2019,
        url = {http://www.gcube-system.org/}
    } 

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)


