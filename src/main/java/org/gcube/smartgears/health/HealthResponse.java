package org.gcube.smartgears.health;

import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.common.health.api.Status;
import org.gcube.common.health.api.response.HealthCheckResponse;
import org.gcube.common.validator.annotations.NotNull;

@JsonInclude(Include.NON_NULL)
public class HealthResponse {
	
	@NotNull
	private Status status;
	
	private List<HealthCheckResponse> checks;

	
	
	public HealthResponse(Status status, List<HealthCheckResponse> checks) {
		super();
		this.status = status;
		this.checks = checks;
	}

	public Status getStatus() {
		return status;
	}

	public List<HealthCheckResponse> getChecks() {
		return checks;
	}
		
}
