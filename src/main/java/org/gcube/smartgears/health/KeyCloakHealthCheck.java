package org.gcube.smartgears.health;

import java.util.Set;

import org.gcube.common.health.api.HealthCheck;
import org.gcube.common.health.api.ReadinessChecker;
import org.gcube.common.health.api.response.HealthCheckResponse;
import org.gcube.smartgears.provider.ProviderFactory;

@ReadinessChecker
public class KeyCloakHealthCheck implements HealthCheck{

	private static final String CHECK_NAME = "authorization-check" ;
	
	public String getName(){
		return CHECK_NAME;
	}
	
	@Override
	public HealthCheckResponse check() {
		try {
			Set<String> contexts = ProviderFactory.provider().containerContext().authorizationProvider().getContexts();
			if (contexts.isEmpty())
				return HealthCheckResponse.builder(CHECK_NAME).down().error("no contexts are defined for the client id provided").build();
			return HealthCheckResponse.builder(CHECK_NAME).up().info(String.format("running contexts are %s", contexts)).build();
		}catch (Exception e) {
			return HealthCheckResponse.builder(CHECK_NAME).down().error(e.getMessage()).build();
		}
	}

}
