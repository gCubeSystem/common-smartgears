package org.gcube.smartgears.health;

import java.util.List;
import java.util.TimerTask;
import java.util.stream.Collectors;

import org.gcube.common.health.api.HealthCheck;
import org.gcube.common.health.api.Status;
import org.gcube.common.health.api.response.HealthCheckResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HealthTask extends TimerTask{

	private static final Logger log = LoggerFactory.getLogger(HealthTask.class); 
	
	private HealthResponse response;
	
	private HealthManager healthManager;
	
	public HealthTask(HealthManager healthManager) {
		this.healthManager = healthManager;
	}
	
	@Override
	public void run() {
		
		List<HealthCheck> checks = healthManager.getChecks();
		
		List<HealthCheckResponse> responses = checks.stream().map(c -> this.wrap(c)).collect(Collectors.toList());
		Status totalStatus = responses.stream().anyMatch(r -> r.getStatus().equals(Status.DOWN)) ? Status.DOWN : Status.UP;
		
		this.response = new HealthResponse(totalStatus, responses);
		log.trace("health task executed with total status {}",totalStatus);
	}

	public HealthResponse getResponse() {
		return response;
	}
	
	private HealthCheckResponse wrap(HealthCheck check){
		try {
			return check.check();
		}catch (Throwable t) {
			return HealthCheckResponse.builder(check.getName()).down().error(t.getMessage()).build();
		}
		
	}
}
