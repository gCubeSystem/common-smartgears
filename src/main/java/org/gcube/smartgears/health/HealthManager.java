package org.gcube.smartgears.health;

import java.util.LinkedList;
import java.util.List;

import org.gcube.common.health.api.HealthCheck;

public class HealthManager {
	
	private List<HealthCheck> checks = new LinkedList<>();
	
	public void register(HealthCheck check){
		checks.add(check);
	}
	
	public List<HealthCheck> getChecks() {
		return checks;
	}
}
