package org.gcube.smartgears.handlers.application.request;

import java.time.Duration;

import org.gcube.smartgears.Constants;
import org.gcube.smartgears.handlers.application.RequestEvent;
import org.gcube.smartgears.handlers.application.RequestHandler;
import org.gcube.smartgears.handlers.application.ResponseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micrometer.core.instrument.Metrics;

public class RequestMetrics extends RequestHandler {

	private static Logger log = LoggerFactory.getLogger(RequestMetrics.class);

	private static ThreadLocal<Long> startCallThreadLocal = new ThreadLocal<Long>();

	private static final String HTTP_REQUEST_METRICS_NAME = "http.server.requests";
	
	@Override
	public String getName() {
		return Constants.request_metrics;
	}

	@Override
	public boolean isUnfiltrable() {
		return true;
	}

	@Override
	public void handleRequest(RequestEvent e) {
		startCallThreadLocal.set(System.currentTimeMillis());
	}

	@Override
	public void handleResponse(ResponseEvent e) {
		try {
			String statusCode = Integer.toString(e.response().getStatus());
			Metrics.timer(HTTP_REQUEST_METRICS_NAME, "status", statusCode).record(Duration.ofMillis(System.currentTimeMillis() - startCallThreadLocal.get()));
			startCallThreadLocal.remove();
		}catch(Throwable t) {
			log.warn("error setting Metrics",t);
		}
	}

}
