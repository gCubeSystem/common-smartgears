package org.gcube.smartgears.handlers.application.request;

import static org.gcube.smartgears.handlers.application.request.RequestError.application_failed_error;
import static org.gcube.smartgears.handlers.application.request.RequestError.application_unavailable_error;
import static org.gcube.smartgears.handlers.application.request.RequestError.invalid_request_error;

import java.util.Objects;
import java.util.Set;

import org.gcube.common.security.ContextBean;
import org.gcube.common.security.ContextBean.Type;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.Constants;
import org.gcube.smartgears.configuration.Mode;
import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.handlers.application.RequestEvent;
import org.gcube.smartgears.handlers.application.RequestHandler;
import org.gcube.smartgears.handlers.application.ResponseEvent;
import org.gcube.smartgears.security.secrets.SecretFactory;
import org.gcube.smartgears.security.secrets.exceptions.SecretNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestValidator extends RequestHandler {

	private static Logger log = LoggerFactory.getLogger(RequestValidator.class);

	private ApplicationContext appContext;

	@Override
	public String getName() {
		return Constants.request_validation;
	}

	@Override
	public void handleRequest(RequestEvent call) {

		log.trace("executing request validator ON REQUEST");

		appContext = call.context();

		SecretManagerProvider.set(getSecret(call));

		validateAgainstLifecycle(call);

		rejectUnauthorizedCalls(call);

		if (appContext.container().configuration().mode()!=Mode.offline) {
			validateScopeCall();
		}

	}

	@Override
	public void handleResponse(ResponseEvent e) {
		log.debug("resetting all the Thread local for this call.");
		SecretManagerProvider.reset();
	}


	private void validateAgainstLifecycle(RequestEvent call) {

		switch(appContext.lifecycle().state()) {

		case stopped :
			application_unavailable_error.fire(); break;

		case failed: 
			application_failed_error.fire(); break;

		default: 
			//nothing to do, but avoids warnings
		}

	}

	private void validateScopeCall() {

		String context = SecretManagerProvider.get().getContext();

		if (context == null) {
			log.warn("rejecting unscoped call to {}",appContext.name());
			invalid_request_error.fire("call is unscoped"); 
		}

		ContextBean bean = new ContextBean(context);

		ContainerConfiguration conf = appContext.container().configuration();
		Set<String> allowedContexts =appContext.authorizationProvider().getContexts();
		if (!allowedContexts.contains(context) && 
				!(conf.authorizeChildrenContext() && bean.is(Type.VRE) 
						&& allowedContexts.contains(bean.enclosingScope().toString()) ) ) {
			log.warn("rejecting call to {} in invalid context {}, allowed context are {}",appContext.name(),context,allowedContexts);
			invalid_request_error.fire(appContext.name()+" cannot be called in scope "+context);
		}
	}

	private void rejectUnauthorizedCalls(RequestEvent call){

		Secret secret = SecretManagerProvider.get();

		if (secret == null){
			log.warn("rejecting call to {}, authorization required",appContext.name());
			RequestError.request_not_authorized_error.fire(appContext.name()+": authorization required");
		} 
	}

	@Override
	public String toString() {
		return getName();
	}


	private Secret getSecret(RequestEvent call){
		
		Secret secret = null;
		for (SecretFactory<? extends Secret> factory: call.context().allowedSecretFactories()) {
			try {
				secret = factory.create(call.request());
				break;
			} catch (SecretNotFoundException e) {
				log.info("authorization for secret {} not found", factory.getSecretClass().getName());
			} catch (Throwable t) {
				log.warn("generic error creating secret {}", factory.getSecretClass().getName(), t);
			}
			
		}
		
		if (Objects.isNull(secret))
			RequestError.request_not_authorized_error.fire("call not authorized");
		
		if (!secret.isValid())
			RequestError.request_not_authorized_error.fire("authorization with secret "+secret.getClass().getSimpleName()+": token not valid ");
		
		if (call.context().container().configuration().checkTokenExpiration() && secret.isExpired())
			RequestError.request_not_authorized_error.fire("authorization with secret "+secret.getClass().getSimpleName()+": token expired ");
			
		return secret;
	}



}
