package org.gcube.smartgears.handlers.container.lifecycle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class LinuxDistributionInfo {

	private static final Logger logger = LoggerFactory.getLogger(LinuxDistributionInfo.class);

	public static final String LSB_RELEASE_COMMAND = "lsb_release -a";
	public static final String OS_RELEASE_FILE_PATH = "/etc/os-release";

	protected Map<String, String> info;

	protected Map<String, String> getInfoViaLsbReleaseCommand() throws IOException {
		logger.trace("Going to exec {}", LSB_RELEASE_COMMAND);
		Process process = Runtime.getRuntime().exec(LSB_RELEASE_COMMAND);
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		Map<String, String> map = parseBufferedReader(bufferedReader);
		bufferedReader.close();
		return map;
	}

	private Map<String, String> parseBufferedReader(BufferedReader bufferedReader) throws IOException {
		Map<String, String> map = new HashMap<>();
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			String[] nameValue = parseLine(line);
			map.put(nameValue[0], nameValue[1]);
		}
		return map;
	}

	private String[] parseLine(String line) {
		String[] splitted = line.split("=");
		if (splitted.length < 2) {
			splitted = line.split(":");
		}
		String[] ret = new String[2];
		ret[0] = splitted[0].trim();
		ret[1] = splitted[1].trim().replace("\"", "");
		return ret;
	}

	private Map<String, String> getInfoViaFile(File file) throws IOException {
		logger.trace("Going to read file {}", file.getAbsolutePath());
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		Map<String, String> map = parseBufferedReader(bufferedReader);
		bufferedReader.close();
		return map;

	}

	protected Map<String, String> getInfoViaOsReleaseFile() throws IOException {
		File osReleaseFile = new File(OS_RELEASE_FILE_PATH);
		return getInfoViaFile(osReleaseFile);
	}

	private Map<String, String> retriveInfo() {
		try {
			return getInfoViaLsbReleaseCommand();
		} catch (IOException e) {
			
		}
		
		try {
			return getInfoViaOsReleaseFile();
		}catch (IOException e) {
			
		}
		
		return null;
	}

	public Map<String, String> getInfo() {
		if (info == null) {
			info = retriveInfo();
		}
		return info;
	}

}
