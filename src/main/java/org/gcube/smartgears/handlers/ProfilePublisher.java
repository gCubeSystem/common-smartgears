package org.gcube.smartgears.handlers;

import java.util.Collection;

public interface ProfilePublisher {
	
	void addTo(Collection<String> contexts);

	void addToAll();
	
	void update();

	void removeFrom(Collection<String> contexts);


}
