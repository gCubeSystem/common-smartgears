package org.gcube.smartgears.publishing;

import java.util.Set;

import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.context.container.ContainerContext;

public interface Publisher {

	/**
	 * creates the container resource in the context
	 * 
	 * @param container 
	 * @param contexts the new contexts where the resource must be created
	 * @return
	 */
	boolean create(ContainerContext container, Set<String> contexts);
	
	/**
	 * creates the application resource in the contexts
	 * 
	 * @param application 
	 * @param contexts the new contexts where the resource must be created
	 * @return
	 */
	boolean create(ApplicationContext application, Set<String> contexts);
	
	
	/**
	 * updates the application resource
	 * 
	 * @param application
	 * @return
	 */
	boolean update(ApplicationContext application);
	
	/**
	 * updates the container resource
	 * 
	 * @param container
	 * @return
	 */
	boolean update(ContainerContext container);
	
	
	/**
	 * removes the application resource from the contexts
	 * 
	 * @param application
	 * @param contexts the contexts from where the resource must be removed
	 * @return
	 */
	boolean remove(ApplicationContext application, Set<String> contexts);
	
	
	/**
	 * removes the container resource from the contexts
	 * @param application 
	 * @param contexts the contexts from where the resource must be removed
	 * @return
	 */
	boolean remove(ContainerContext application, Set<String> contexts);
}
