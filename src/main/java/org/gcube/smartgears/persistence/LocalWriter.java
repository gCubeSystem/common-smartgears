package org.gcube.smartgears.persistence;

import static org.gcube.smartgears.utils.Utils.fileAt;
import static org.gcube.smartgears.utils.Utils.notNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.gcube.smartgears.configuration.ComponentConfiguration;
import org.gcube.smartgears.configuration.ConfiguredWith;

@ConfiguredWith(LocalWriterConfiguration.class)
public class LocalWriter implements PersistenceWriter {

	private String location;
	
	@Override
	public void configure(ComponentConfiguration configuration) {
		this.location = ((LocalWriterConfiguration) configuration).getLocation();
		
	}
	
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	

	@Override
	public File writefile(String path) {
		
		notNull("relative path", path);
		
		return fileAt(new File(location, path).getAbsolutePath()).toWrite();
	}
	
	@Override
	public File file(String path) {
		
		notNull("relative path", path);
		
		return fileAt(new File(location, path).getAbsolutePath()).toRead();
	}
	
		
	public void validate() {
		
		File locationDir = new File(location);
		if (!(locationDir.exists() && locationDir.isDirectory() && locationDir.canRead() && locationDir.canWrite()))
				throw new IllegalStateException("invalid node configuration: home "+location+" does not exist or is not a directory or cannot be accessed in read/write mode");
	
	}

	@Override
	public long getFreeSpace() {
		try {
			return Files.getFileStore(Paths.get(location)).getUsableSpace();
		}catch (Exception e) {
			return -1;
		}
	}
	
}
