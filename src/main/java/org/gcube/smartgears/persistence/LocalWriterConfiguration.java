package org.gcube.smartgears.persistence;

import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;
import org.gcube.smartgears.configuration.ComponentConfiguration;

public class LocalWriterConfiguration implements ComponentConfiguration{

	@NotEmpty @NotNull
	private String location;

	protected LocalWriterConfiguration() {}
	
	public LocalWriterConfiguration(String location) {
		super();
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
		
		
}
