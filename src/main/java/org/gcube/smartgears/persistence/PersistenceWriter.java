package org.gcube.smartgears.persistence;

import java.io.File;

import org.gcube.smartgears.configuration.Configurable;

public interface PersistenceWriter extends Configurable{
		
	File file(String path);
	
	File writefile(String path);
	
	long getFreeSpace();
	
	String getLocation();

}