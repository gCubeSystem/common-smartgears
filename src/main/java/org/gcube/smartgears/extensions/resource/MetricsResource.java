package org.gcube.smartgears.extensions.resource;

import static org.gcube.smartgears.Constants.plain_text;
import static org.gcube.smartgears.extensions.HttpExtension.Method.GET;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.gcube.smartgears.extensions.ApiResource;
import org.gcube.smartgears.extensions.ApiSignature;

import io.micrometer.core.instrument.Metrics;
import io.micrometer.prometheus.PrometheusMeterRegistry;

public class MetricsResource extends ApiResource {

	private static final long serialVersionUID = 1L;
	
	public static final String mapping = "/metrics";
	
	private static final ApiSignature signature = handles(mapping).with(method(GET).produces(plain_text)); 
		
	
	MetricsResource() {
		super(signature);
	}
	
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrometheusMeterRegistry registry = (PrometheusMeterRegistry) Metrics.globalRegistry.getRegistries().stream().findFirst().get(); 
		registry.scrape(resp.getWriter());
	}

}