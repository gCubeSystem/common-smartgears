package org.gcube.smartgears.extensions.resource;

import static org.gcube.smartgears.Constants.application_json;
import static org.gcube.smartgears.extensions.HttpExtension.Method.GET;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.extensions.ApiResource;
import org.gcube.smartgears.extensions.ApiSignature;

/**
 * An {@link ApiResource} of {@link RemoteResource} at {@link #mapping}.
 * 
 * @author Fabio Simeoni
 *
 */
public class ConfigurationResource extends ApiResource {

	private static final long serialVersionUID = 1L;
	
	public static final String mapping = "/configuration";
	
	private static final ApiSignature signature = handles(mapping).with(method(GET).produces(application_json)); 
	
	ConfigurationResource() {
		super(signature);
	}
		
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		ApplicationConfiguration config = context().configuration();
		new ObjectMapper().writeValue(resp.getWriter(), config); 
	}

}
