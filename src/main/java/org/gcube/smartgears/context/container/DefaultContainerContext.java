package org.gcube.smartgears.context.container;

import static org.gcube.smartgears.Constants.container_profile_file_path;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.UUID;

import org.gcube.common.events.Hub;
import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.smartgears.configuration.PersistenceConfiguration;
import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.context.Properties;
import org.gcube.smartgears.lifecycle.container.ContainerLifecycle;
import org.gcube.smartgears.persistence.PersistenceWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default {@link ContainerContext} implementation.
 * 
 * @author Fabio Simeoni
 *
 */
public class DefaultContainerContext implements ContainerContext {

	private static Logger log = LoggerFactory.getLogger(DefaultContainerContext.class);
	
	private final ContainerConfiguration configuration;
	private final ContainerLifecycle lifecycle;
	private final Properties properties;
	private final Hub hub;
	private final AuthorizationProvider authorizationProvider;
	private final String id;
	private final PersistenceWriter persistenceWriter;
	/**
	 * Creates an instance with mandatory parameters.
	 * @param configuration the configuration
	 * @param hub the event hub
	 * @param lifecycle the lifecycle
	 * @param properties the properties
	 */
	public DefaultContainerContext(ContainerConfiguration configuration, Hub hub, ContainerLifecycle lifecycle, AuthorizationProvider authProvider,
			Properties properties) {
		this.configuration=configuration;
		this.hub=hub;
		this.lifecycle = lifecycle;
		this.properties=properties;
		this.authorizationProvider = authProvider;
		
		PersistenceConfiguration persistenceWriterConf = configuration.persistenceConfiguration();
		
		try {
			persistenceWriter = persistenceWriterConf.getImplementationClass().getDeclaredConstructor().newInstance();
			persistenceWriter.configure(persistenceWriterConf.getWriterConfiguration());
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		File file = persistenceWriter.file(container_profile_file_path);

		String id = null;
		if (file.exists()) {
			log.info("loading persisted state for container");
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
				id = (String) ois.readObject();
			} catch (Exception e) {
				log.error("error loading persisted state, creating new uuid", e);
			}

		}
		if (id == null) {
			id = UUID.randomUUID().toString();
			log.info("container id created is {}", id);

		}
		
		this.id = id;
		
	}
	
	/*
	public HostingNode profile() {
		return properties().lookup(container_profile_property).value(HostingNode.class);
	};*/

	@Override
	public ContainerConfiguration configuration() {
		return configuration;
	}

	@Override
	public ContainerLifecycle lifecycle() {
		return lifecycle;
	}

	@Override
	public Hub events() {
		return hub;
	}

	@Override
	public PersistenceWriter persistenceWriter() {
		return persistenceWriter;
	}

	@Override
	public Properties properties() {
		return properties;
	}

	@Override
	public String id() {
		return id;
	}
	
	public AuthorizationProvider authorizationProvider() {
		return authorizationProvider;
	}
	
}
