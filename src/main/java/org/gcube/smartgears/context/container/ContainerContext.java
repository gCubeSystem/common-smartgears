package org.gcube.smartgears.context.container;

import org.gcube.common.events.Hub;
import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.context.Properties;
import org.gcube.smartgears.lifecycle.container.ContainerLifecycle;
import org.gcube.smartgears.persistence.PersistenceWriter;

/**
 * The management context of the container.
 * 
 * @author Fabio Simeoni
 *
 */
public interface ContainerContext {


	/**
	 * Returns the configuration of the container.
	 * @return the configuration
	 */
	ContainerConfiguration configuration();
	
	
	/**
	 * Returns the lifecycle of the container
	 * @return the lifecycle
	 */
	ContainerLifecycle lifecycle();
	
	/**
	 * Returns the event hub of the container
	 * @return the hub
	 */
	Hub events();
	
	/**
	 * Returns the persistence manager of the container.
	 * @return the manager
	 */
	PersistenceWriter persistenceWriter();
	
	/**
	 * Returns the properties of the container.
	 * @return the properties
	 */
	Properties properties();
	
	String id();

	
	AuthorizationProvider authorizationProvider();
	

}
