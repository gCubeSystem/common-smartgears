package org.gcube.smartgears.context.application;

import java.nio.file.Path;
import java.util.List;

import org.gcube.common.events.Hub;
import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.context.Properties;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.lifecycle.application.ApplicationLifecycle;
import org.gcube.smartgears.persistence.PersistenceWriter;
import org.gcube.smartgears.security.secrets.SecretFactory;

import jakarta.servlet.ServletContext;

/**
 * The management context of an application.
 * 
 * @author Fabio Simeoni
 * 
 */
public interface ApplicationContext {


	String id();
	
	/**
	 * Returns the name of the application.
	 * 
	 * @return the name
	 */
	String name();

	/**
	 * Returns the configuration of the application.
	 * 
	 * @return the configuration
	 */
	ApplicationConfiguration configuration();

	/**
	 * Returns the lifecycle of the application.
	 * 
	 * @return the lifecycle
	 */
	ApplicationLifecycle lifecycle();

	/**
	 * Returns the event hub of the application
	 * 
	 * @return the hub
	 */
	Hub events();

	/**
	 * Returns the persistence manager of the application.
	 * 
	 * @return the manager
	 */
	PersistenceWriter persistence();

	/**
	 * Returns the servlet context of the application.
	 * 
	 * @return the context
	 */
	ServletContext application();

	/**
	 * Returns the management context of the container.
	 * 
	 * @return the context
	 */
	ContainerContext container();

	/**
	 * Returns the properties of the application
	 * 
	 * @return the properties
	 */
	Properties properties();
	
	/**
	 * Returns the authorization provider.
	 * @return the AuhtorizationProvider 
	 **/
	AuthorizationProvider authorizationProvider(); 
	
	/**
	 * Returns the Path to the configuration Folder for the current app
	 * 
	 * @return the Path to the folder , null if the Path is not present
	 */
	Path appSpecificConfigurationFolder();
	
	/**
	 * returns the list of factory secrets ordered by priority
	 * 
	 * @returnA Linked List
	 */
	List<SecretFactory<? extends Secret>> allowedSecretFactories();
	
}
