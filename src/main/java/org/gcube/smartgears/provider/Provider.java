package org.gcube.smartgears.provider;

import java.util.List;

import jakarta.servlet.ServletContext;

import org.gcube.smartgears.configuration.SmartgearsConfiguration;
import org.gcube.smartgears.configuration.application.ApplicationHandlers;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.extensions.ApplicationExtension;
import org.gcube.smartgears.handlers.container.ContainerHandler;
import org.gcube.smartgears.publishing.Publisher;

/**
 * Provides dependencies for container and application management.
 * 
 * @author Fabio Simeoni
 *
 */
public interface Provider {

	//container-level dependencies

	/**
	 * Returns the runtime properties.
	 * @return the properties.
	 
	SmartGearsConfiguration smartgearsConfiguration();
	*/
	
	/**
	 * Assembles and returns the context of the container.
	 * @return the container's context
	 */
	ContainerContext containerContext();
	
	/**
	 * Returns the handlers associated with the container.
	 * @return the handlers
	 */
	List<ContainerHandler> containerHandlers();
	
	
	/**
	 * Returns an implementation of the IS publisher for the container
	 * @return the publisher implementation
	 */
	List<Publisher> publishers();
	
	//application-level dependencies
	
	/**
	 * Assembles and returns the context of a given application.
	 * @param container the context of the container
	 * @param application the servlet context of the application
	 * @return
	 */
	
	ApplicationContext contextFor(ContainerContext container,ServletContext application);
	
	/**
	 * Returns the handlers associated with a given application.
	 * @param application the context of the application
	 * @return the handlers
	 */
	ApplicationHandlers handlersFor(ApplicationContext application);
	
	List<ApplicationExtension> extensionsFor(ApplicationContext application);
	
	SmartgearsConfiguration smartgearsConfiguration();

}
