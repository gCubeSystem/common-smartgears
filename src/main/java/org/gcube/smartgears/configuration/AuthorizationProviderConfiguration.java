package org.gcube.smartgears.configuration;

import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.factories.AuthorizationProviderFactory;
import org.gcube.common.validator.annotations.IsValid;
import org.gcube.common.validator.annotations.NotNull;

public class AuthorizationProviderConfiguration {

	@NotNull
	AuthorizationProviderFactory<?> authProviderFactory;
	
	@NotNull @IsValid
	Credentials credentials;
	
	public AuthorizationProviderConfiguration(AuthorizationProviderFactory<?> authProviderFactory,
			Credentials credentials) {
		super();
		this.authProviderFactory = authProviderFactory;
		this.credentials = credentials;
	}

	public AuthorizationProviderFactory<?> getAuthProviderFactory() {
		return authProviderFactory;
	}

	public Credentials getCredentials() {
		return credentials;
	}

	@Override
	public String toString() {
		return "AuthorizationProviderConfiguration [authProviderFactory=" + authProviderFactory.getClass() + "]";
	}
	
	
}
