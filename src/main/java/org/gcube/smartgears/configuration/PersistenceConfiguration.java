package org.gcube.smartgears.configuration;

import org.gcube.com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.common.validator.annotations.IsValid;
import org.gcube.common.validator.annotations.NotNull;
import org.gcube.smartgears.persistence.PersistenceWriter;

@JsonInclude(value = Include.NON_NULL)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class PersistenceConfiguration {

	@NotNull
	private Class<? extends PersistenceWriter> implementationClass;
	
	@IsValid
	private ComponentConfiguration writerConfiguration;

	protected PersistenceConfiguration() {}
	
	public <T extends ComponentConfiguration> PersistenceConfiguration(Class<? extends PersistenceWriter> implementationClass, T writerConfiguration) {
		super();
		this.implementationClass = implementationClass;
		this.writerConfiguration = writerConfiguration;
	}

	public Class<? extends PersistenceWriter> getImplementationClass() {
		return this.implementationClass;
	}

	public ComponentConfiguration getWriterConfiguration() {
		return writerConfiguration;
	}
		
}
