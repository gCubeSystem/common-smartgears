package org.gcube.smartgears.configuration;

public interface Configurable {

	void configure(ComponentConfiguration configuration);
}
