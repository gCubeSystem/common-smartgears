package org.gcube.smartgears.configuration;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(include=As.PROPERTY, use=Id.CLASS, property= "className")
public interface ComponentConfiguration {

	String getLocation();
}
