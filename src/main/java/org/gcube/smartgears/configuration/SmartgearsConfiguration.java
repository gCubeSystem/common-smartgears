package org.gcube.smartgears.configuration;

public class SmartgearsConfiguration {

	private String version;
	
	public SmartgearsConfiguration(String version) {
		super();
		this.version = version;
	}

	public String getVersion() {
		return version;
	}
}
