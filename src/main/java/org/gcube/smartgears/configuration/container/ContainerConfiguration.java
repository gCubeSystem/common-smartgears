package org.gcube.smartgears.configuration.container;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.validator.ValidationError;
import org.gcube.common.validator.Validator;
import org.gcube.common.validator.ValidatorFactory;
import org.gcube.common.validator.annotations.IsValid;
import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;
import org.gcube.smartgears.configuration.AuthorizationProviderConfiguration;
import org.gcube.smartgears.configuration.Mode;
import org.gcube.smartgears.configuration.PersistenceConfiguration;
import org.gcube.smartgears.configuration.ProxyAddress;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;

/**
 * The configuration of the container.
 *  
 * @author Fabio Simeoni
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContainerConfiguration {

	@NotNull  @IsValid
	private BaseConfiguration baseConfiguration;

	@IsValid
	private Map<String,String> properties = new HashMap<String, String>();

	@NotNull @IsValid
	private Site site;

	@IsValid
	private ProxyAddress proxy;

	@NotEmpty @NotNull
	private String accountingFallbackLocation;

	private List<ApplicationConfiguration> apps = new ArrayList<ApplicationConfiguration>();

	

	@NotNull @IsValid
	private PersistenceConfiguration persistenceConfiguration;

	@NotNull @IsValid
	private AuthorizationProviderConfiguration authorizationConfiguration;
	
	
	protected void setBaseConfiguration(BaseConfiguration baseConfiguration) {
		this.baseConfiguration = baseConfiguration;
	}

	protected void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	protected void setSite(Site site) {
		this.site = site;
	}

	protected void setProxy(ProxyAddress proxy) {
		this.proxy = proxy;
	}

	protected void setAccountingFallbackLocation(String accountingFallbackLocation) {
		this.accountingFallbackLocation = accountingFallbackLocation;
	}

	protected void setPersistenceConfiguration(PersistenceConfiguration persistenceConfiguration) {
		this.persistenceConfiguration = persistenceConfiguration;
	}

	protected void setAuthorizationProviderConfiguration(
			AuthorizationProviderConfiguration authorizationConfiguration) {
		this.authorizationConfiguration = authorizationConfiguration;
	}

	public void setApps(List<ApplicationConfiguration> apps) {
		this.apps = apps;
	}

	/**
	 * Returns the management mode for the container.
	 * @return the management mode
	 */
	public Mode mode() {
		return baseConfiguration.getMode();
	}
	
	public boolean checkTokenExpiration() {
		return baseConfiguration.checkTokenExpiration(); 
	}

	/**
	 * Returns the application configurations included in this configuration.  
	 * @return the application configurations
	 */
	public List<ApplicationConfiguration> apps() {
		return apps;
	}

	/**
	 * Returns the configuration of an application with a given context path.
	 * @param context the context path
	 * @return the application configuration
	 */
	public ApplicationConfiguration app(String context) {

		for (ApplicationConfiguration app : apps)
			if (context.equals(app.context()))
				return app;

		return null;
	}

	/**
	 * Adds the configuration of an application to this configuration.
	 * @param app the application configuration
	 * @return this configuration
	 */
	public synchronized ContainerConfiguration app(ApplicationConfiguration app) {
		int indexToRemove =-1;
		int index =0;
		for (ApplicationConfiguration application : apps){
			if (app.context().equals(application.context()))
				indexToRemove = index;
			index++;
		}
		if(indexToRemove!=-1)
			apps.remove(indexToRemove);
		apps.add(app);
		return this;
	}

	/**
	 * Returns the geographical site of the container.
	 * @return the site
	 */
	public Site site() {
		return site;
	}


	/**
	 * Returns the infrastructure in which the container is running.
	 * @return the infrastructure
	 */
	public String infrastructure() {
		return baseConfiguration.getInfrastructure();
	}


	/**
	 * Returns the host name of the container.
	 * @return the host name;
	 */
	public String hostname() {
		return baseConfiguration.getHostname();
	}

	/**
	 * Returns the port at which the container is listening for requests.
	 * @return the port
	 */
	public int port() {
		return baseConfiguration.getPort();
	}


	/**
	 * Returns the port at which the container is listening for requests.
	 * @return the port
	 */
	public String protocol() {
		return baseConfiguration.getProtocol();
	}


	public boolean authorizeChildrenContext() {
		return baseConfiguration.isAuthorizeChildrenContext();
	}

	/**
	 * Returns the proxy of the container.
	 * @return the proxy
	 */
	public ProxyAddress proxy() {
		return proxy;
	}
	
	
	/**
	 * Returns the persistence manager of the container.
	 * @return the manager
	 */
	public PersistenceConfiguration persistenceConfiguration() {
		return this.persistenceConfiguration;
	}
	
	/**
	 * Returns the persistence manager of the container.
	 * @return the manager
	 */
	public String accountingFallbackLocation() {
		return accountingFallbackLocation;
	}

	/**
	 * Returns the authorization configuration.
	 * @return AuthorizationProviderConfiguration the configuration
	 */
	public AuthorizationProviderConfiguration authorizationConfiguration() {
		return authorizationConfiguration;
	}
	
	/**
	 * Returns the configuration properties of the container.
	 * @return the properties
	 */
	public Map<String,String> properties() {
		return Collections.unmodifiableMap(properties);
	}

	/**
	 * Returns the publication frequency for the container's profile.
	 * @return the frquency;
	 */
	public long publicationFrequency() {
		return baseConfiguration.getPublicationFrequencyInSeconds();
	}

	/**
	 * Validates this configuration
	 * 
	 * @throws IllegalStateException if the configuration is invalid
	 */
	public void validate() {

		List<String> msgs = new ArrayList<String>();

		Validator validator = ValidatorFactory.validator();

		for (ValidationError error : validator.validate(this))
			msgs.add(error.toString());
		
		if (!msgs.isEmpty())
			throw new IllegalStateException("invalid configuration: "+msgs);

	}

	@Override
	public String toString() {
		return "ContainerConfiguration [baseConfiguration=" + baseConfiguration + ", properties=" + properties
				+ ", site=" + site + ", proxy=" + proxy + ", accountingFallbackLocation=" + accountingFallbackLocation
				+ ", persistence=" + persistenceConfiguration.getImplementationClass().getSimpleName()
				+ ", authorizationProvider=" + authorizationConfiguration + "]";
	}

	
	
}