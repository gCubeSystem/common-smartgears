package org.gcube.smartgears.configuration.container;

import static org.gcube.smartgears.Constants.default_container_publication_frequency_in_seconds;

import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;
import org.gcube.smartgears.configuration.Mode;

public class BaseConfiguration {

	
	Mode mode = Mode.online;
	
	@NotNull @NotEmpty
	String hostname;
		
	@NotNull
	Integer port;
	
	@NotNull @NotEmpty
	String protocol="http";
	
	boolean authorizeChildrenContext = false;
	
	@NotNull @NotEmpty
	String infrastructure;
	
	long publicationFrequencyInSeconds = default_container_publication_frequency_in_seconds;
	
	@NotNull @NotEmpty
	private Boolean checkTokenExpiration = false;
	
	public boolean checkTokenExpiration() {
		return checkTokenExpiration; 
	}
	
	public Mode getMode() {
		return mode;
	}

	public String getHostname() {
		return hostname;
	}

	public Integer getPort() {
		return port;
	}

	public String getProtocol() {
		return protocol;
	}

	public boolean isAuthorizeChildrenContext() {
		return authorizeChildrenContext;
	}

	public String getInfrastructure() {
		return infrastructure;
	}



	public long getPublicationFrequencyInSeconds() {
		return publicationFrequencyInSeconds;
	}

	public void setPublicationFrequencyInSeconds(long publicationFrequencyInSeconds) {
		this.publicationFrequencyInSeconds = publicationFrequencyInSeconds;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public void setAuthorizeChildrenContext(boolean authorizeChildrenContext) {
		this.authorizeChildrenContext = authorizeChildrenContext;
	}

	public void setInfrastructure(String infrastructure) {
		this.infrastructure = infrastructure;
	}


	@Override
	public String toString() {
		return "BaseConfiguration [mode=" + mode + ", hostname=" + hostname + ", port=" + port + ", protocol="
				+ protocol + ", authorizeChildrenContext=" + authorizeChildrenContext + ", infrastructure="
				+ infrastructure + ", publicationFrequency=" + publicationFrequencyInSeconds
				+ "]";
	}
	
	
	
}
