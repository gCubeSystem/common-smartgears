package org.gcube.smartgears.configuration.container;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.factories.AuthorizationProviderFactory;
import org.gcube.smartgears.configuration.AuthorizationProviderConfiguration;
import org.gcube.smartgears.configuration.ComponentConfiguration;
import org.gcube.smartgears.configuration.ConfiguredWith;
import org.gcube.smartgears.configuration.PersistenceConfiguration;
import org.gcube.smartgears.configuration.ProxyAddress;
import org.gcube.smartgears.configuration.SmartgearsConfiguration;
import org.gcube.smartgears.handlers.container.ContainerHandler;
import org.gcube.smartgears.handlers.container.lifecycle.AccountingManager;
import org.gcube.smartgears.handlers.container.lifecycle.ContainerProfileManager;
import org.gcube.smartgears.persistence.LocalWriter;
import org.gcube.smartgears.persistence.LocalWriterConfiguration;
import org.gcube.smartgears.persistence.PersistenceWriter;
import org.gcube.smartgears.security.defaults.DefaultAuthorizationProviderFactory;
import org.gcube.smartgears.utils.Utils;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

/**
 * Binds {@link ContainerConfiguration}s to and from XML serialisations.
 * 
 * @author Fabio Simeoni
 * 
 */
public class ContainerConfigurationBinder {

	public ContainerConfiguration load(InputStream stream) {
		try {
			Ini configurator = new Ini(stream);
			ContainerConfiguration conf = new ContainerConfiguration();

			Section nodeSection = configurator.get("node");
			if (nodeSection != null) {
				BaseConfiguration nodeConf = new BaseConfiguration();
				nodeSection.to(nodeConf);
				conf.setBaseConfiguration(nodeConf);
			}

			Section propertiesSection = configurator.get("properties");
			if (propertiesSection != null)
				conf.setProperties(propertiesSection.entrySet().stream()
						.collect(Collectors.toMap(Entry::getKey, Entry::getValue)));

			Section siteSection = configurator.get("site");
			if (siteSection != null) {
				Site siteConf = new Site();
				siteSection.to(siteConf);
				conf.setSite(siteConf);
			}

			initAuthorizationPart(configurator, conf);

			initPersistencePart(configurator, conf);

			initProxyPart(configurator, conf);

			// TODO: find a solution for this shit
			String location = Utils.home() + "/state";
			File dir = new File(location);
			if (!dir.exists())
				dir.mkdirs();
			conf.setAccountingFallbackLocation(location);
			// END Shit

			return conf;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void initProxyPart(Ini configurator, ContainerConfiguration conf) throws Exception {
		Section proxySection = configurator.get("proxy");
		if (proxySection != null) {
			ProxyAddress proxyConf = new ProxyAddress();
			proxySection.to(proxyConf);
			conf.setProxy(proxyConf);
		}
	}

	@SuppressWarnings("unchecked")
	private void initPersistencePart(Ini configurator, ContainerConfiguration conf) throws Exception {
		Section persistenceSection = configurator.get("persistence");
		if (persistenceSection != null) {
			String type = persistenceSection.get("class");
			if (type == null)
				throw new Exception("ini file error: type not found in \"persistence\" section");
			/*
			 * PersistenceWriter persistenceWriter; try { Object persistenceImpl =
			 * Class.forName(type).getDeclaredConstructor().newInstance(); persistenceWriter
			 * = PersistenceWriter.class.cast(persistenceImpl); }catch (Exception e) { throw
			 * new
			 * Exception("ini file error: invalid persistence type in \"persistence\" section"
			 * , e); }
			 */
			// persistenceSection.to(persistenceWriter);

			Class<? extends PersistenceWriter> persistenceClass = null;
			try {
				persistenceClass = (Class<? extends PersistenceWriter>) Class.forName(type);
			} catch (Exception e) {
				throw new Exception("ini file error: invalid persistence type in \"persistence\" section", e);
			}
			if (!persistenceClass.isAnnotationPresent(ConfiguredWith.class))
				throw new Exception(
						"ini file error: invalid class type in \"persistence\" section,ConfiguredWith annotation not present");
			Class<? extends ComponentConfiguration> writerConfClass = persistenceClass
					.getAnnotation(ConfiguredWith.class).value();
			ComponentConfiguration writerConfiguration = writerConfClass.getDeclaredConstructor().newInstance();
			persistenceSection.to(writerConfiguration, ".");
			conf.setPersistenceConfiguration(
					new PersistenceConfiguration((Class<PersistenceWriter>) persistenceClass, writerConfiguration));
		} else {
			String location = Utils.home() + "/state";
			File dir = new File(location);
			if (!dir.exists())
				dir.mkdirs();

			conf.setPersistenceConfiguration(
					new PersistenceConfiguration(LocalWriter.class, new LocalWriterConfiguration(location)));
		}

	}

	private void initAuthorizationPart(Ini configurator, ContainerConfiguration conf) throws Exception {
		Section authorizationSection = configurator.get("authorization");
		if (authorizationSection != null) {

			String provider = authorizationSection.get("factory");
			AuthorizationProviderFactory<?> authProviderFactory;
			if (provider != null) {
				try {
					Object authProviderImpl = Class.forName(provider).getDeclaredConstructor().newInstance();
					authProviderFactory = AuthorizationProviderFactory.class.cast(authProviderImpl);
				} catch (Exception e) {
					throw new Exception("ini file error: invalid provider type in \"authorization\" section", e);
				}
			} else
				authProviderFactory = new DefaultAuthorizationProviderFactory();

			authorizationSection.to(authProviderFactory, "factory.");

			String type = authorizationSection.get("credentials.class");
			if (type == null)
				throw new Exception("ini file error: credentials type not found in \"authorization\" section");
			Credentials credentials;
			try {
				Object credentialsImpl = Class.forName(type).getDeclaredConstructor().newInstance();
				credentials = Credentials.class.cast(credentialsImpl);
			} catch (Exception e) {
				throw new Exception("ini file error: invalid credentials type in \"authorization\" section", e);
			}
			authorizationSection.to(credentials, "credentials.");

			conf.setAuthorizationProviderConfiguration(
					new AuthorizationProviderConfiguration(authProviderFactory, credentials));
		}
	}

	/**
	 * Returns the handlers of the container from their XML serialisation.
	 * 
	 * @param stream the serialisation
	 * @return the handlers
	 * @throws RuntimeException if the serialisation is invalid
	 */
	public List<ContainerHandler> bindHandlers(ClassLoader classloader) {

		LinkedList<ContainerHandler> handlers = new LinkedList<ContainerHandler>();

		// ADDING BASE Handlers (order is important)
		handlers.add(new AccountingManager());
		handlers.add(new ContainerProfileManager());

		handlers.addAll(scanForContainerHadlers(classloader));

		return handlers;

	}

	private List<? extends ContainerHandler> scanForContainerHadlers(ClassLoader classloader) throws RuntimeException {

		// TODO: scan for Container Handler

		return Collections.emptyList();
	}

	public SmartgearsConfiguration loadSmartgearsProperty() {
		try {
		Ini configurator = new Ini(this.getClass().getResourceAsStream("/META-INF/smartgears-config.ini"));
		String version = configurator.get("smartgears").get("version");
		return new SmartgearsConfiguration(version);
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
