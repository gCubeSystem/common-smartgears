package org.gcube.smartgears.configuration.application;

import java.io.File;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.smartgears.configuration.PersistenceConfiguration;
import org.gcube.smartgears.handlers.application.ApplicationLifecycleHandler;
import org.gcube.smartgears.handlers.application.RequestHandler;
import org.gcube.smartgears.handlers.application.lifecycle.ApplicationProfileManager;
import org.gcube.smartgears.handlers.application.request.RequestAccounting;
import org.gcube.smartgears.handlers.application.request.RequestMetrics;
import org.gcube.smartgears.handlers.application.request.RequestValidator;
import org.gcube.smartgears.persistence.LocalWriter;
import org.gcube.smartgears.persistence.LocalWriterConfiguration;
import org.gcube.smartgears.utils.Utils;
import org.yaml.snakeyaml.Yaml;

/**
 * Binds {@link ApplicationConfiguration}s to and from XML serialisations.
 * 
 * @author Fabio Simeoni
 * 
 */
public class ApplicationConfigurationBinder {

	/**
	 * Returns the application configuration from its XML serialisation.
	 * 
	 * @param stream the serialisation
	 * @return the configuration
	 * @throws RuntimeException if the serialisation is invalid
	 */
	public ApplicationConfiguration load(InputStream stream) {
		try {
			Yaml yaml = new Yaml();
			ObjectMapper mapper = new ObjectMapper();
			String mapAsString = mapper.writeValueAsString(yaml.load(stream));

			System.out.println(mapAsString);
			
			ApplicationConfiguration conf = mapper.readValue(mapAsString, ApplicationConfiguration.class);

			if (conf.persistenceConfiguration() == null) {
				String location = String.format("%s/state/%s_%s", Utils.home(), conf.group(), conf.name());
				File dir = new File(location);
				if (!dir.exists())
					dir.mkdirs();

				conf.persistenceConfiguration(
						new PersistenceConfiguration(LocalWriter.class, new LocalWriterConfiguration(location)));

			}

			return conf;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns the handlers of the application from their XML serialisation.
	 * 
	 * @param stream the serialisation
	 * @return the handlers
	 * @throws RuntimeException if the serialisation is invalid
	 */
	public ApplicationHandlers bindHandlers(ClassLoader classLoader) {

		List<RequestHandler> requestHandlers = new LinkedList<RequestHandler>();

		// ADDING BASE Handler (order is important)
		requestHandlers.add(new RequestMetrics());
		requestHandlers.add(new RequestValidator());
		requestHandlers.add(new RequestAccounting());

		// TODO scan RequestHAndler form classloader

		List<ApplicationLifecycleHandler> lifecycleHandlers = new LinkedList<ApplicationLifecycleHandler>();

		// ADDING BASE Handler (order is important)
		lifecycleHandlers.add(new ApplicationProfileManager());

		// TODO scan ApplicationLifecycleHandler form classloader

		return new ApplicationHandlers(lifecycleHandlers, requestHandlers);

	}

	/**
	 * Returns the extensions of the application from their XML serialisation.
	 * 
	 * @param stream the serialisation
	 * @return the extensions
	 * @throws RuntimeException if the serialisation is invalid
	 * 
	 *                          public ApplicationExtensions
	 *                          bindExtensions(InputStream stream) {
	 * 
	 *                          //collects handler classes Set<Class<?>> classes =
	 *                          scanForExtensions();
	 * 
	 *                          try {
	 * 
	 *                          JAXBContext ctx =
	 *                          JAXBContext.newInstance(classes.toArray(new
	 *                          Class<?>[0]));
	 * 
	 *                          return (ApplicationExtensions)
	 *                          ctx.createUnmarshaller().unmarshal(stream);
	 * 
	 *                          } catch (JAXBException e) {
	 * 
	 *                          throw unchecked(e);
	 * 
	 *                          } finally { closeSafely(stream); } }
	 * 
	 * 
	 * 
	 *                          private Set<Class<?>> scanForExtensions() throws
	 *                          RuntimeException {
	 * 
	 *                          @SuppressWarnings("all")
	 *                          ServiceLoader<ApplicationExtension> handlerLoader =
	 *                          (ServiceLoader)
	 *                          ServiceLoader.load(ApplicationExtension.class);
	 * 
	 *                          Set<Class<?>> scanned = new HashSet<Class<?>>();
	 * 
	 *                          for (ApplicationExtension handler : handlerLoader) {
	 *                          Class<?> handlerClass = handler.getClass(); if
	 *                          (handlerClass.isInterface() ||
	 *                          handlerClass.getModifiers() == Modifier.ABSTRACT)
	 *                          continue; else scanned.add(handlerClass); }
	 * 
	 *                          //add top-level configuration
	 *                          scanned.add(ApplicationExtensions.class);
	 * 
	 *                          return scanned; }
	 */

	public void scanForApplicationHandlers(ClassLoader currentClassLoader) {
		// TODO Auto-generated method stub
	}

}
