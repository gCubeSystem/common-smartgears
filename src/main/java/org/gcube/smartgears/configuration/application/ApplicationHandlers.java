package org.gcube.smartgears.configuration.application;

import java.util.LinkedList;
import java.util.List;

import org.gcube.smartgears.handlers.application.ApplicationHandler;
import org.gcube.smartgears.handlers.application.ApplicationLifecycleHandler;
import org.gcube.smartgears.handlers.application.RequestHandler;

/**
 * The {@link ApplicationHandler}s that manage the application.
 *  
 * @author Fabio Simeoni
 *
 */
public class ApplicationHandlers {

	
	private List<ApplicationLifecycleHandler> lifecycleHandlers = new LinkedList<ApplicationLifecycleHandler>();
	
	private List<RequestHandler> requestHandlers = new LinkedList<RequestHandler>();
	
	public ApplicationHandlers(List<ApplicationLifecycleHandler> lifecycleHandlers, List<RequestHandler> requestHandlers) {
		this.lifecycleHandlers = lifecycleHandlers;
		this.requestHandlers = requestHandlers;
	}
	
	/**
	 * Returns the {@link ApplicationLifecycleHandler}s for the service.
	 * @return the lifecycle handlers
	 */
	public List<ApplicationLifecycleHandler> lifecycleHandlers() {
		return lifecycleHandlers;
	}
	
	/**
	 * Sets the {@link ApplicationLifecycleHandler}s for the service.
	 * @param handlers the lifecycle handlers
	 * @return this configuration
	 */
	public ApplicationHandlers setLifecycleHandlers(List<ApplicationLifecycleHandler> handlers) {
		this.lifecycleHandlers = handlers;
		return this;
	}
	
	/**
	 * Returns the {@link RequestHandler}s for the service.
	 * @return the lifetime handlers
	 */
	public List<RequestHandler> requestHandlers() {
		return requestHandlers;
	}
	
	/**
	 * Sets the {@link RequestHandler}s for the service.
	 * @param handlers the request handlers
	 * @return this configuration
	 */
	public ApplicationHandlers setRequetHandlers(List<RequestHandler> handlers) {
		this.requestHandlers = handlers;
		return this;
	}
  
 	
 	public void mergeWith(ApplicationHandlers other){
 		List<ApplicationLifecycleHandler> lifecycles = other.lifecycleHandlers();
 		for (ApplicationLifecycleHandler handler : lifecycles)
 			if (!this.lifecycleHandlers().contains(handler))
 				this.lifecycleHandlers().add(handler);
		
 		List<RequestHandler> requests = other.requestHandlers();
 		for (RequestHandler handler : requests)
 			if (!this.requestHandlers().contains(handler))
 				this.requestHandlers().add(handler);
 
 	}
   
}