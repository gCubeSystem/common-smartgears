package org.gcube.smartgears.configuration.application;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.validator.annotations.NotEmpty;

public class GCubeExclude {

	@NotEmpty
	private List<String> handlers = new ArrayList<String>();
	
	@NotEmpty
	private String path;

	public List<String> getHandlers() {
		return handlers;
	}

	public String getPath() {
		return path;
	}

	protected GCubeExclude() {}

	public GCubeExclude(String path) {
		super();
		this.path = path;
	}
	
	public GCubeExclude(List<String> handlers, String path) {
		super();
		this.handlers = handlers;
		this.path = path;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GCubeExclude other = (GCubeExclude) obj;
		if (handlers == null) {
			if (other.handlers != null)
				return false;
		} else if (!handlers.equals(other.handlers))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Exclude [handlers=" + handlers + ", path=" + path + "]";
	}
		
}
