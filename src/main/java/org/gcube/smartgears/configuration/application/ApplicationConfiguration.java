package org.gcube.smartgears.configuration.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.gcube.com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.common.validator.ValidationError;
import org.gcube.common.validator.Validator;
import org.gcube.common.validator.ValidatorFactory;
import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;
import org.gcube.smartgears.configuration.PersistenceConfiguration;

/**
 * The configuration of a managed app.
 * <p>
 * Includes the list of its client services.
 *  
 * @author Lucio Lelii
 *
 */
@JsonInclude(value = Include.NON_NULL)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class ApplicationConfiguration {

	@NotNull
	String name;

	@NotNull
	String group;

	@NotNull
	String version;
	
	String description="";
	
	@JsonIgnore
	String context;
	
	private boolean proxable = true;
		
	Set<GCubeExclude> excludes= new HashSet<>();
	
	Set<GCubeInclude> includes= new HashSet<>();
	
	@NotEmpty @JsonProperty("persistence")
	PersistenceConfiguration persistenceConfiguration; 
	
	@JsonProperty("allowed-secrets") 
	List<String> allowedSecretClasses = null;
	
	public Set<GCubeExclude> excludes() {
		return excludes;
	}
	
	public Set<GCubeInclude> includes() {
		return includes;
	}
	
	public ApplicationConfiguration() {}
			
	public String name() {
		return name;
	}
	
	public String context() {
		return context;
	}
	
	public ApplicationConfiguration excludes(GCubeExclude ... excludes) {
		this.excludes=new HashSet<GCubeExclude>(Arrays.asList(excludes));
		return this;
	}
	
	public ApplicationConfiguration includes(GCubeInclude... includes) {
		this.includes=new HashSet<GCubeInclude>(Arrays.asList(includes));
		return this;
	}
	
	public ApplicationConfiguration allowedSecrets(String ... classNames) {
		this.allowedSecretClasses = Arrays.asList(classNames);
		return this;
	}
	
	public List<String> allowedSecrets(){
		return this.allowedSecretClasses;
	}
	
	public ApplicationConfiguration context(String context) {
		this.context = context;
		return this;
	}
	
	public ApplicationConfiguration name(String name) {
		this.name=name;
		return this;
	}
	
	public ApplicationConfiguration persistenceConfiguration(PersistenceConfiguration configuration) {
		this.persistenceConfiguration = configuration;
		return this;
	}

	public ApplicationConfiguration proxable(boolean proxable) {
		this.proxable = proxable;
		return this;
	}
	
	public String group() {
		return group;
	}
	
	
	public ApplicationConfiguration group(String group) {
		this.group=group;
		return this;
	}
	
	public String version() {
		return version;
	}
	
	public ApplicationConfiguration version(String version) {
		this.version=version;
		return this;
	}

	
	public String description() {
		return description;
	}
	
	public ApplicationConfiguration description(String description) {
		this.description=description;
		return this;
	}

	public boolean proxable() {
		return proxable;
	}
		
	public PersistenceConfiguration persistenceConfiguration() {
		return persistenceConfiguration;
	}
		
	public void validate() {
		
		List<String> msgs = new ArrayList<String>();
		
		Validator validator = ValidatorFactory.validator();
		
		for (ValidationError error : validator.validate(this))
			msgs.add(error.toString());
		
		if (!this.excludes().isEmpty() && !this.includes().isEmpty())
			msgs.add("exclude tags and includes tags are mutually exclusive");
		
		if (!msgs.isEmpty())
			throw new IllegalStateException("invalid configuration: "+msgs);
	}

	@Override
	public int hashCode() {
		return Objects.hash(description, excludes, group, includes, name, proxable, version, allowedSecretClasses);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationConfiguration other = (ApplicationConfiguration) obj;
		return Objects.equals(description, other.description)
				&& Objects.equals(excludes, other.excludes) && Objects.equals(group, other.group)
				&& Objects.equals(includes, other.includes) && Objects.equals(name, other.name)
				&& proxable == other.proxable && Objects.equals(version, other.version);
	}

	@Override
	public String toString() {
		return "ApplicationConfiguration [name=" + name + ", group=" + group + ", version=" + version + ", description="
				+ description + ", context=" + context + ", proxable=" + proxable + ", excludes=" + excludes
				+ ", includes=" + includes + ", persistenceConfiguration=" + persistenceConfiguration
				+ ", allowedSecretClasses=" + this.allowedSecretClasses + "]";
	}
	
	
}