package org.gcube.smartgears.configuration;

import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;

public class ProxyAddress {

	@NotNull @NotEmpty
	String protocol = "http";
		
	@NotNull @NotEmpty
	String hostname;
	
	@NotNull
	Integer port;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	
}
