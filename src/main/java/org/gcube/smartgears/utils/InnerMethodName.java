package org.gcube.smartgears.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InnerMethodName {

	private static Logger logger = LoggerFactory.getLogger(InnerMethodName.class);
	
	// Thread local variable containing each thread's ID
    private static final InheritableThreadLocal<String> threadMethod =
        new InheritableThreadLocal<String>() {
    	    	
    	@Override protected String initialValue() {
                return null;
        }
    	
    };
	
	private InnerMethodName(){}
    
	public static String get(){
		String calledMethod = threadMethod.get();
		logger.trace("getting InnerMethodName as "+calledMethod+" in thread "+Thread.currentThread().getId() );
		return calledMethod;
	}
	
	public static void set(String calledMethod){
		if (calledMethod==null) return;
		threadMethod.set(calledMethod);
		logger.trace("setting InnerMethodName as "+calledMethod+" in thread "+Thread.currentThread().getId() );
	}
	
	public static void reset(){
		threadMethod.remove();
	}
}
