package org.gcube.smartgears.security.defaults;

import java.util.List;

import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.factories.AuthorizationProviderFactory;
import org.gcube.common.validator.ValidationError;
import org.gcube.common.validator.ValidatorFactory;
import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.smartgears.security.SimpleCredentials;

public class DefaultAuthorizationProviderFactory implements AuthorizationProviderFactory<DefaultAuthorizationProvider>{

	@NotEmpty
	private String endpoint;
	
	@Override
	public DefaultAuthorizationProvider connect(Credentials credentials) {
		if (!SimpleCredentials.class.isInstance(credentials))
			throw new IllegalArgumentException("invalid credential type passed");
		List<ValidationError> errors = ValidatorFactory.validator().validate(credentials);
		if (!errors.isEmpty())
			throw new IllegalArgumentException(String.format("invalid credential: %s", errors));
		if (this.endpoint == null || this.endpoint.isEmpty())
			throw new IllegalArgumentException("invalid enpoint passed");
		return new DefaultAuthorizationProvider((SimpleCredentials)credentials, this.endpoint);
	}

	@Override
	public String toString() {
		return "DefaultAuthorizationProviderFactory [endpoint=" + endpoint + "]";
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
	
}
