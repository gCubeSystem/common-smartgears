package org.gcube.smartgears.security.secrets;

import org.gcube.common.security.secrets.UmaTokenSecret;
import org.gcube.smartgears.Constants;
import org.gcube.smartgears.security.secrets.exceptions.SecretNotFoundException;

import jakarta.servlet.http.HttpServletRequest;

public class GCubeKeyCloakSecretFactory implements SecretFactory<UmaTokenSecret> {

	private static final String BEARER_AUTH_PREFIX ="Bearer"; 
	
	@Override
	public UmaTokenSecret create(HttpServletRequest request) throws SecretNotFoundException {
		String authHeader  = request.getHeader(Constants.authorization_header);
		String umaToken = null;
		if (authHeader!=null && !authHeader.isEmpty() && authHeader.startsWith(BEARER_AUTH_PREFIX)) {
				umaToken = authHeader.substring(BEARER_AUTH_PREFIX.length()).trim();
				return new UmaTokenSecret(umaToken);
		} else throw new SecretNotFoundException();
	}
	
	@Override
	public Class<UmaTokenSecret> getSecretClass() {
		return UmaTokenSecret.class;
	}

}
