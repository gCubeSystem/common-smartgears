package org.gcube.smartgears.security.secrets;

import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.security.secrets.exceptions.SecretNotFoundException;

import jakarta.servlet.http.HttpServletRequest;

public interface SecretFactory<T extends Secret> {

	Class<T> getSecretClass();
	
	T create(HttpServletRequest request) throws SecretNotFoundException;

}
