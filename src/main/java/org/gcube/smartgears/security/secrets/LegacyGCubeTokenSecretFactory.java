package org.gcube.smartgears.security.secrets;

import static org.gcube.smartgears.Constants.token_header;

import java.util.Objects;

import org.gcube.common.security.secrets.GCubeSecret;
import org.gcube.smartgears.security.secrets.exceptions.SecretNotFoundException;

import jakarta.servlet.http.HttpServletRequest;

public class LegacyGCubeTokenSecretFactory implements SecretFactory<GCubeSecret> {

	@Override
	public GCubeSecret create(HttpServletRequest request) throws SecretNotFoundException {
		String token = request.getParameter(token_header)==null? request.getHeader(token_header):request.getParameter(token_header);
		if (Objects.isNull(token) || token.isBlank()) throw new SecretNotFoundException();
		return new GCubeSecret(token);
	}

	@Override
	public Class<GCubeSecret> getSecretClass() {
		return GCubeSecret.class;
	}

}
