package org.gcube.smartgears.probe;

import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import org.gcube.smartgears.managers.ContainerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebListener
public class ContainerListener implements ServletContextListener {
	
	public static Logger log = LoggerFactory.getLogger(ContainerListener.class);

	public void contextDestroyed(jakarta.servlet.ServletContextEvent sce) {
		log.trace("shutting down container from probe");
		ContainerManager.instance.stop(true);
	};
	
	public void contextInitialized(jakarta.servlet.ServletContextEvent sce) {
		log.trace("starting up probe...");
	};
}

