package test;

import java.io.InputStream;

import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.configuration.application.ApplicationConfigurationBinder;
import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.configuration.container.ContainerConfigurationBinder;
import org.junit.Test;

public class BinderTest {
	
	@Test 
	public void bindContainerConfig() throws Exception {
		InputStream stream = BinderTest.class.getResourceAsStream("/container.ini");
		
		ContainerConfiguration conf = new ContainerConfigurationBinder().load(stream);
		System.out.println(conf.toString());
	}
	
	
	@Test 
	public void bindApplicationConfig() throws Exception {
		InputStream stream = BinderTest.class.getResourceAsStream("/application.yaml");
		
		ApplicationConfiguration conf = new ApplicationConfigurationBinder().load(stream);
		System.out.println(conf.toString());
	}
}
