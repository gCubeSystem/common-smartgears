package utils;

import java.io.File;
import java.util.List;

import jakarta.servlet.ServletContext;

import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.configuration.application.ApplicationHandlers;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.extensions.ApplicationExtension;
import org.gcube.smartgears.provider.DefaultProvider;

public class TestProvider extends DefaultProvider {

	public TestProvider(File configFile) {
		super(configFile);
	}
	
	public ApplicationContext context;
	public ApplicationConfiguration configuration;
	public ApplicationHandlers handlers;
	public List<ApplicationExtension> extensions;
	
	
	public void use(ApplicationConfiguration configuration) {
		this.configuration=configuration;
	}
	
	public void use(ApplicationHandlers handlers) {
		this.handlers=handlers;
	}
	
	public void use(List<ApplicationExtension> extensions) {
		this.extensions=extensions;
	}
	
	
	/*
	@Override
	public SmartGearsConfiguration smartgearsConfiguration() {
		SmartGearsConfiguration conf = new SmartGearsConfiguration();
		conf.version("0.0.1-TEST");
		return conf ;
	}*/

		
	@Override
	public ApplicationContext contextFor(ContainerContext container,ServletContext application) {
		return context = super.contextFor(container,application);
	}
	
	@Override
	public ApplicationHandlers handlersFor(ApplicationContext context) {
		return handlers==null?super.handlersFor(context):handlers;
	}
	
	@Override
	public List<ApplicationExtension> extensionsFor(ApplicationContext context) {
		return extensions==null?super.extensionsFor(context):extensions;
	}
}
