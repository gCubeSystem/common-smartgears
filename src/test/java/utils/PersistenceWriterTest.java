package utils;

import java.io.File;

import org.gcube.common.validator.annotations.IsValid;
import org.gcube.common.validator.annotations.NotNull;
import org.gcube.smartgears.configuration.ComponentConfiguration;
import org.gcube.smartgears.persistence.PersistenceWriter;

public class PersistenceWriterTest implements PersistenceWriter{

	@IsValid @NotNull
	String location;
	
	@Override
	public File file(String path) {
		return new File(location+"/"+path);
	}

	@Override
	public File writefile(String path) {
		return new File(location+"/"+path);
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public long getFreeSpace() {
		return 0;
	}

	@Override
	public void configure(ComponentConfiguration configuration) {
				
	}
	
}
