package utils;

import java.io.InputStream;
import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.smartgears.configuration.PersistenceConfiguration;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.configuration.application.GCubeExclude;
import org.gcube.smartgears.persistence.LocalWriter;
import org.gcube.smartgears.persistence.LocalWriterConfiguration;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import junit.framework.Assert;

public class ConfigurationTest {

	static final String yamlConf = "name: test\n"
			+ "group: group\n"
			+ "version: 1.0.0\n"
			+ "description: pippo\n"
			+ "proxable: true\n"
			+ "excludes:\n"
			+ "- path: /pippo/*\n"
			+ "- handlers: [H1, H2]\n"
			+ "  path: /trip\n"
			+ "persistence:\n"
			+ "  implementationClass: org.gcube.smartgears.persistence.LocalWriter\n"
			+ "  writerConfiguration:\n"
			+ "    className: org.gcube.smartgears.persistence.LocalWriterConfiguration\n"
			+ "    location: /tmp";
	
	static final String yamlConfBase = "name: test\n"
			+ "group: group\n"
			+ "version: 1.0.0\n"
			+ "description: pippo";
	
	@Test
	public void deserialize() {
		Yaml yaml = new Yaml();
		InputStream inputStream = this.getClass()
		 .getClassLoader()
		 .getResourceAsStream("applicationTest.yaml");
		yaml.load(inputStream);

	}
	
	@Test
	public void serialize() throws Exception{
		ApplicationConfiguration configuration =new ApplicationConfiguration();
		configuration.name("test").group("group").description("pippo").version("1.0.0").excludes(new GCubeExclude("/pippo/*"), new GCubeExclude(List.of("H1","H2"), "/trip"));
		configuration.persistenceConfiguration(new PersistenceConfiguration(LocalWriter.class, new LocalWriterConfiguration("/tmp")));
		/*
		ObjectMapper mapper = new ObjectMapper();
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(configuration);
		System.out.println(value);
		Map<String, Object> res = mapper.readValue(value, Map.class);
		
		
		
		Yaml yaml = new Yaml();
		String yamlValue = yaml.dump(res);
		System.out.println(yamlValue);
		*/
		
		Yaml yaml = new Yaml();
		ObjectMapper mapper = new ObjectMapper();
		String mapAsString = mapper.writeValueAsString(yaml.load(yamlConf));
		System.out.println(mapAsString);
		
		ApplicationConfiguration conf2 = mapper.readValue(mapAsString, ApplicationConfiguration.class);
		Assert.assertTrue(configuration.equals(conf2));
	}

	@Test
	public void serializeBase() throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		Yaml yaml = new Yaml();
		
		String mapAsString = mapper.writeValueAsString(yaml.load(yamlConfBase));
		System.out.println(mapAsString);
		
		ApplicationConfiguration conf2 = mapper.readValue(mapAsString, ApplicationConfiguration.class);
		Assert.assertEquals("test", conf2.name());

	}
}
